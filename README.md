th the Drush modueinfo you will be able to see some information about the selected module.

Normally if you want so see some info about an installed modules version, you would do something like

<code> cat sites/all/modules/contrib/views/*info</code>

With Drush moduleinfo, you can do a

<code> drush mi views</code>

And you will get something like

<code>
 Name               Views
 Description        Create customized lists and queries from your database.
 Installed version  6.x-2.12
 Newest version     6.x-3.0-rc3
 Module path        sites/all/modules/contrib/views
 Dependencies       None
</code>


