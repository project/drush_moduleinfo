<?php

/**
 * Implements hook_drush_command().
 */
function moduleinfo_drush_command() {
  $items = array();

  $items['moduleinfo'] = array(
    'description' => 'Show info about module',
    'aliases' => array('mi'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'module-name' => 'The module of which you want info',
    ),
    'examples' => array(
      'drush mi ctools' => 'Show module information of ctool',
    ),
  );

  return $items;
}

/**
 * Implements validate function for drush command.
 */
function drush_moduleinfo_validate($alias) {
  drush_set_option('module-name', $alias);
}

/**
 * Implements custom drush command.
 */
function drush_moduleinfo() {
  $dependency_list = 'None';
  $module = drush_get_option('module-name', NULL);
  $rsc = drush_db_select('system', 'filename', 'name=:name', array(':name' => $module));
  $filename = drush_db_result($rsc);
  $info_file = drupal_parse_info_file(dirname($filename) . '/' . $module . '.info');
  if (isset($info_file['dependencies'])) {
    $dependency_list = implode("\n", $info_file['dependencies']);
  }
    $xml = _drush_pm_get_release_history_xml(array('name' => $module, 'drupal_version' => $info_file['core']));
  drush_print('');
  drush_print_table(array(
    array('Name', $info_file['name']),
    array('Description', $info_file['description']),
    array('Installed version', $info_file['version']),
    array('Newest version', $xml->releases->release[0]->version),
    array('Module path', dirname($filename)),
    array('Dependencies', $dependency_list),
  ),TRUE);

}

